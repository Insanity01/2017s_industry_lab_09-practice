package ictgradschool.industry.lab09.ex01;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * A class that creates some lists of stuff and then filters them based on whether items in those lists meet some
 * predicated conditions.
 */
public class ExerciseOne {

    /**
     * Overall program flow is in this method.
     */
    private void start() {

        List<String> strings = new ArrayList<>();
        strings.add("Hello");
        strings.add("Hello");
//        strings.add(Math.PI);
        strings.add("HELLO");

        List<Integer> ints = new ArrayList<>();
        ints.add(3);
        ints.add(32222);
//        ints.add("Not an actual number");
        ints.add(2);
        ints.add(1);
        ints.add(22);

//        Filter the list of Strings, removing any which aren't entirely uppercase

        filterList(strings, new UpperCaseStringPredicate());

//        Filter the list of Integers, removing any which aren't odd numbers
        filterList(ints, new OddNumbersPredicate());

        // Print out the items left in the strings list - should just be "HELLO".
        System.out.println("Strings:");
        for (Object obj : strings) {
            String s = (String) obj;
            System.out.println(" - " + s);
        }
        System.out.println();

        // Print out the items left in the ints list - should be 3 and 397.
        System.out.println("Integers:");
        for (Object obj : ints) {
            Integer i = (Integer) obj;
            System.out.println(" - " + i);
        }

    }

    /**
     * This method iterates through the list and removes all items in the list which don't satisfy the given predicate.
     *
     * @param list
     * @param predicate
     */
    private <T> void filterList(List<T> list, IPredicate predicate) {

        Iterator<T> iter = list.iterator();

        while (iter.hasNext()) {
            Object obj = iter.next();
            if (!predicate.test(obj)) {
                iter.remove();
            }
        }
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {
        new ExerciseOne().start();
    }
}
